<?php

namespace Drupal\media_indiveo\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a value represents a valid Indiveo video URL.
 *
 * @Constraint(
 *   id = "valid_indiveo_url",
 *   label = @Translation("Valid Indiveo URL"),
 *   type = {"string"}
 * )
 */
class ValidIndiveoUrlConstraint extends Constraint {

  /**
   * The error message if the URL is empty.
   *
   * @var string
   */
  public $emptyUrlMessage = 'The URL cannot be empty.';

  /**
   * The error message if the URL does not match.
   *
   * @var string
   */
  public $invalidUrlMessage = 'The given URL is not valid. Valid Indiveo video URLs start with https://indiveo.service/embed/.';

}
