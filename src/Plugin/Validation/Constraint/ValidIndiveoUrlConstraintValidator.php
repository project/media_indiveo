<?php

namespace Drupal\media_indiveo\Plugin\Validation\Constraint;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media_indiveo\Plugin\media\Source\Indiveo;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates Indiveo video URLs.
 */
class ValidIndiveoUrlConstraintValidator extends ConstraintValidator {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    /** @var \Drupal\media\MediaInterface $media */
    $media = $value->getEntity();
    $source = $media->getSource();
    if (!($source instanceof Indiveo)) {
      throw new \LogicException('Media source must implement ' . Indiveo::class);
    }

    $url = $source->getSourceFieldValue($media);
    $url = trim($url, '/');
    // The URL may be NULL if the source field is empty, which is invalid input.
    if (empty($url)) {
      $this->context->addViolation($constraint->emptyUrlMessage);
      return;
    }

    if (!str_starts_with($url, 'https://indiveo.services/embed/')) {
      $this->context->addViolation($constraint->invalidUrlMessage);
    }
  }

}
