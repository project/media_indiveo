<?php

namespace Drupal\media_indiveo\Plugin\media\Source;

use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaSourceFieldConstraintsInterface;

/**
 * A media source plugin for non-OEmbed remote media assets.
 *
 * @MediaSource(
 *   id = "media_indiveo",
 *   label = @Translation("Indiveo Video"),
 *   description = @Translation("A media source plugin for Invideo videos."),
 *   allowed_field_types = {"string"},
 *   default_thumbnail_filename = "video.png"
 * )
 */
class Indiveo extends MediaSourceBase implements MediaSourceFieldConstraintsInterface {

  /**
   * Key for "Name" metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_NAME = 'name';

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      static::METADATA_ATTRIBUTE_NAME => $this->t('Name'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $url = $media->get($this->configuration['source_field'])->value;
    // If the source field is not required, it may be empty.
    if (empty($url)) {
      return parent::getMetadata($media, $attribute_name);
    }
    switch ($attribute_name) {
      case static::METADATA_ATTRIBUTE_NAME:
      case 'default_name':
        return ucfirst(str_replace(['https://indiveo.services/embed/', '/'], ['', ' '], trim($url, '/')));

      default:
        return parent::getMetadata($media, $attribute_name);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints() {
    return [
      'valid_indiveo_url' => [],
    ];
  }

}
